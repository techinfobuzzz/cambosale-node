const express       = require('express');
const Router        = express.Router();
const DB            = require('../../models/db');
const HELPERFUNC    = require('../../models/commonfunctions');

Router.get('/listDistricts',function(req,res) {
  const response = {
    status  : 0,
  }
  DB.GetDocument('districts', {}, {}, {}, function(err, result) {
      if(err) {
          res.send(response);
      } else {
          response.status  = 1;
          response.data    = result;
          response.count   = result.length;
          res.send(response);
      }
  });
});

Router.get('/listCities',function(req,res) {
  const response = {
    status  : 0,
  }
  DB.GetDocument('cities', {}, {}, {}, function(err, result) {
      if(err) {
          res.send(response);
      } else {
          response.status  = 1;
          response.data    = result;
          res.send(response);
      }
  });
});

Router.post('/viewDistrict',function(req,res) {
  const response = {
    status  : 0,
  }
  DB.GetOneDocument('districts', {_id:req.body.id}, {}, {}, function(err, result) {
      if(err) {
          res.send(response);
      } else {
          DB.GetDocument('cities', {districtId:req.body.id}, {}, {}, function(err, result1) {
                if(err) {
                    res.send(response);
                } else {
                response.status  = 1;
                response.data    = result;
                response.cities  = result1;
                res.send(response);
            }
          });
      }
  });
});

Router.post('/addUpdateDistrict',function(req,res) {
  const response = {
    status  : 0,
    message : 'Something went wrong in your code!'
  }
  req.checkBody('district', 'district is required.').notEmpty();
  var errors = req.validationErrors();
  if (errors) {
    return res.status(422).json({ errors: errors});
  }
  const district = req.body.district;
  const faqFormData = {
    district  : HELPERFUNC.Capitalize(district),
    slug      : req.body.slug,
    state     : req.body.state,
    status    : req.body.status
  }
  if(req.body.id=="0"){
    DB.GetOneDocument('districts', {slug : req.body.slug}, {}, {}, function(err, result) {
      if(result){
        response.status  = 0;
        response.message = 'Data you have entered is already exist!';
        res.send(response);
      } else {
        DB.InsertDocument('districts', faqFormData, function(err, result1) {
          if(err) {
            res.send(response);
          } else {
            response.status  = 1;
            response.message = 'District added successfully';
            response.id      = result1._id;
            res.send(response);
          }
        });
      }
    });
  } else {
    DB.FindUpdateDocument('districts',{_id:req.body.id}, faqFormData, function(err, result) {
      if(err) {
        res.send(response);
      } else {
        DB.GetOneDocument('districts', {_id:req.body.id}, {}, {}, function(err, result1) {
            if(err) {
                res.send(response);
            } else {
                DB.GetDocument('cities', {districtId:req.body.id}, {}, {}, function(err, result2) {
                  if(err) {
                      res.send(response);
                  } else {
                    const data = {
                      id         : result1._id,
                      district   : result1.district,
                      state      : result1.state,
                      slug       : result1.slug,
                      status     : result1.status
                    }
                    response.status  = 1;
                    response.message = 'District updated successfully';
                    response.data    = data;
                    response.cities  = result2;
                    res.send(response);
                }
              });
            }
        });
      }
    });
  }
})

Router.post('/deleteDistrict',function(req,res) {
  const response = {
    status  : 0,
    message : 'Something went wrong in your code!'
  }
  DB.DeleteDocument('districts', {_id:req.body.id}, function(err, result) {
      if(err) {
          res.send(response);
      } else {
        DB.GetDocument('districts', {}, {}, {}, function(err, result) {
            if(err) {
                res.send(response);
            } else {
                  response.status  = 1;
                  response.message = 'District deleted successfully';
                  response.data    = result;
                  response.count   = result.length;
                  res.send(response);
            }
        });
      }
  });
})

Router.post('/addUpdateCity',function(req,res) {
  const response = {
    status  : 0,
    message : 'Something went wrong in your code!'
  }
  req.checkBody('city', 'city is required.').notEmpty();
  var errors = req.validationErrors();
  if (errors) {
    return res.status(422).json({ errors: errors});
  }
  const city = req.body.city;
  const faqFormData = {
    city        : HELPERFUNC.Capitalize(city),
    slug        : req.body.slug,
    districtId  : req.body.districtId,
    status      : req.body.status
  }
  if(req.body.id=="0"){
        DB.InsertDocument('cities', faqFormData, function(err, result) {
          if(err) {
            res.send(response);
          } else {
              DB.GetDocument('cities', {districtId:req.body.districtId}, {}, {}, function(err, result1) {
                if(err) {
                    res.send(response);
                } else {
                      response.status  = 1;
                      response.data    = result1
                      response.message = 'City added successfully';
                    res.send(response);
                }
            });
          }
        });
  } else {
    DB.FindUpdateDocument('cities',{_id:req.body.id}, faqFormData, function(err, result) {
      if(err) {
        res.send(response);
      } else {
        DB.GetDocument('cities', {districtId:req.body.districtId}, {}, {}, function(err, result1) {
            if(err) {
                res.send(response);
            } else {
                  response.status  = 1;
                  response.data    = result1
                  response.message = 'City updated successfully';
                res.send(response);
            }
        });
      }
    });
  }
})

Router.post('/deleteCity',function(req,res) {
  const response = {
    status  : 0,
    message : 'Something went wrong in your code!'
  }
  DB.DeleteDocument('cities', {_id:req.body.id}, function(err, result) {
      if(err) {
          res.send(response);
      } else {
        DB.GetDocument('cities', {districtId:req.body.districtId}, {}, {}, function(err, result) {
            if(err) {
                res.send(response);
            } else {
                  response.status  = 1;
                  response.message = 'City deleted successfully';
                  response.data    = result;
                  res.send(response);
            }
        });
      }
  });
})

module.exports = Router;