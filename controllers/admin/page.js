const express       = require('express');
const Router        = express.Router();
const DB            = require('../../models/db');
const Functions        = require('../../models/commonfunctions');

Router.get('/listPages',function(req,res) {
  const response = {
    status  : 0,
  }
  DB.GetDocument('pages', {}, {}, {}, function(err, result) {
      if(err) {
          res.send(response);
      } else {
            response.status  = 1;
            response.data    = result;
            response.count   = result.length;
            res.send(response);
      }
  });
});

Router.post('/viewPage',function(req,res) {
  const response = {
    status  : 0,
  }
  DB.GetOneDocument('pages', {_id:req.body.id}, {}, {}, function(err, result) {
      if(err) {
          res.send(response);
      } else {
            response.status  = 1;
            response.data    = result;
            res.send(response);
      }
  });
});

Router.post('/addUpdatePage',function(req,res) {
  const response = {
    status  : 0,
    message : 'Something went wrong in your code!'
  }
  req.checkBody('name', 'name is required.').notEmpty();
  req.checkBody('description', 'description is required.').notEmpty();
  var errors = req.validationErrors();
  if (errors) {
    return res.status(422).json({ errors: errors});
  }
  const name         = req.body.name;
  const slug         = Functions.CreateSlug(name);
  const description  = req.body.description;
  const status       = req.body.status;
  const pagesFormData = {
    name         : Functions.Capitalize(name),
    slug         : slug,
    description  : Functions.Capitalize(description),
    status       : status
  }
  if(req.body.id=="0"){
    DB.GetOneDocument('pages', {slug : slug}, {}, {}, function(err, result) {
      if(result){
        response.status  = 0;
        response.message = 'Data you have entered is already exist!';
        res.send(response);
      } else {
        DB.InsertDocument('pages', pagesFormData, function(err, result1) {
          if(err) {
            res.send(response);
          } else {
            response.status  = 1;
            response.message = 'Page added successfully';
            response.id      = result1._id;
            res.send(response);
          }
        });
      }
    });
  } else {
    DB.FindUpdateDocument('pages',{_id:req.body.id}, pagesFormData, function(err, result) {
      if(err) {
        res.send(response);
      } else {
        DB.GetOneDocument('pages', {_id:req.body.id}, {}, {}, function(err, result1) {
            if(err) {
                res.send(response);
            } else {
                  const pagesData = {
                    id           : result1._id,
                    name         : result1.name,
                    slug         : result1.slug,
                    description  : result1.description,
                    status       : result1.status
                  }
                  response.status  = 1;
                  response.message = 'Page updated successfully';
                  response.data    = pagesData;
                res.send(response);
            }
        });
      }
    });
  }
})

Router.post('/deletePage',function(req,res) {
  const response = {
    status  : 0,
    message : 'Something went wrong in your code!'
  }
  DB.DeleteDocument('pages', {_id:req.body.id}, function(err, result) {
      if(err) {
          res.send(response);
      } else {
        DB.GetDocument('pages', {}, {}, {}, function(err, result) {
            if(err) {
                res.send(response);
            } else {
                  response.status  = 1;
                  response.message = 'Page deleted successfully';
                  response.data    = result;
                  response.count   = result.length;
                  res.send(response);
            }
        });
      }
  });
})

module.exports = Router;
