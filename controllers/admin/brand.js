const express       = require('express');
const Router        = express.Router();
const DB            = require('../../models/db');
const HELPERFUNC    = require('../../models/commonfunctions');
const UPLOAD        = require('../../models/fileupload');

Router.get('/listBrands',function(req,res) {
  const response = {
    status  : 0,
  }
  DB.GetDocument('brands', {}, {}, {}, function(err, result) {
      if(err) {
          res.send(response);
      } else {
          response.status  = 1;
          response.data    = result;
          res.send(response);
      }
  });
});

Router.post('/viewBrand',function(req,res) {
  const response = {
    status  : 0,
  }
  DB.GetOneDocument('brands', {_id:req.body.id}, {}, {}, function(err, result) {
      if(err) {
          res.send(response);
      } else {
          response.status  = 1;
          response.data    = result;
          res.send(response);
      }
  });
});

Router.post('/addUpdateBrand',UPLOAD.single('image'),function(req,res) {
  const response = {
    status  : 0,
    message : 'Something went wrong in your code!'
  }
  req.checkBody('name', 'name is required.').notEmpty();
  var errors = req.validationErrors();
  if (errors) {
    return res.status(422).json({ errors: errors});
  }
  const name        = HELPERFUNC.Capitalize(req.body.name);
  const description = req.body.description;
  const status      = req.body.status;
  const formData = {
    name        : name,
    status      : status
  }
  if(req.file){
    formData.image  = req.file.filename
  } else if(req.body.image){
    formData.image  = req.body.image
  }
  if(req.body.id=="0"){
    DB.GetOneDocument('brands', {name : name}, {}, {}, function(err, result) {
      if(result){
        response.status  = 0;
        response.message = 'Brand name already exist!';
        res.send(response);
      } else {
        DB.InsertDocument('brands', formData, function(err, result1) {
          if(err) {
            res.send(response);
          } else {
            response.status  = 1;
            response.message = 'Brand added successfully';
            response.id      = result1._id;
            res.send(response);
          }
        });
      }
    });
  } else {
    DB.FindUpdateDocument('brands',{_id:req.body.id}, formData, function(err, result) {
      if(err) {
        res.send(response);
      } else {
        DB.GetOneDocument('brands', {_id:req.body.id}, {}, {}, function(err, result1) {
            if(err) {
                res.send(response);
            } else {
                  const data = {
                    id            : result1._id,
                    name          : result1.name,
                    image         : result1.image?result1.image:"",
                    status        : result1.status
                  }
                  response.status  = 1;
                  response.message = 'Brand updated successfully';
                  response.data    = data;
                res.send(response);
            }
        });
      }
    });
  }
})

Router.post('/deleteBrand',function(req,res) {
  const response = {
    status  : 0,
    message : 'Something went wrong in your code!'
  }
  DB.DeleteDocument('brands', {_id:req.body.id}, function(err, result) {
      if(err) {
          res.send(response);
      } else {
        DB.GetDocument('brands', {}, {}, {}, function(err, result1) {
            if(err) {
                res.send(response);
            } else {
                  response.status  = 1;
                  response.message = 'Brand deleted successfully';
                  response.data    = result1;
                  res.send(response);
            }
        });
      }
  });
})

module.exports = Router;
