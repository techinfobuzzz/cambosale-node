const express       = require('express');
const Router        = express.Router();
const DB            = require('../../models/db');
const UPLOAD        = require('../../models/fileupload');
const HELPERFUNC    = require('../../models/commonfunctions');

Router.get('/listMaincategories',function(req,res) {
  const response = {
    status  : 0,
  }
  DB.GetDocument('maincategories', {}, {}, {}, function(err, result) {
      if(err) {
          res.send(response);
      } else {
          response.status  = 1;
          response.data    = result;
          response.count   = result.length;
          res.send(response);
      }
  });
});

Router.post('/viewMaincategory',function(req,res) {
  const response = {
    status  : 0,
  }
  DB.GetOneDocument('maincategories', {_id:req.body.id}, {}, {}, function(err, result) {
      if(err) {
          res.send(response);
      } else {
          response.status  = 1;
          response.data    = result;
          res.send(response);
      }
  });
});

var cpUpload = UPLOAD.fields([{ name: 'image', maxCount: 1 },{ name: 'icon', maxCount: 1 }])
Router.post('/addUpdateMaincategory',cpUpload,function(req,res) {
  const response = {
    status  : 0,
    message : 'Something went wrong in your code!'
  }
  req.checkBody('name', 'name is required.').notEmpty();
  var errors = req.validationErrors();
  if (errors) {
    return res.status(422).json({ errors: errors});
  }
  const name     = req.body.name;
  const status   = req.body.status;
  const formData = {
    // parentID    : parentID,
    name        : HELPERFUNC.Capitalize(name),
    status      : status
  }
  if(req.files['icon']){
    formData.icon  = req.files['icon'][0].filename
  } else {
    formData.icon  = req.body.icon
  }
  if(req.files['image']){
    formData.image  = req.files['image'][0].filename
  } else {
    formData.image  = req.body.image
  }
  if(req.body.id=="0"){
    DB.GetOneDocument('maincategories', {name : name}, {}, {}, function(err, result) {
      if(result){
        response.status  = 0;
        response.message = 'Data you have entered is already exist!';
        res.send(response);
      } else {
        DB.InsertDocument('maincategories', formData, function(err, result1) {
          if(err) {
            res.send(response);
          } else {
            response.status  = 1;
            response.message = 'Main Category added successfully';
            response.id      = result1._id;
            res.send(response);
          }
        });
      }
    });
  } else {
    DB.FindUpdateDocument('maincategories',{_id:req.body.id}, formData, function(err, result) {
      if(err) {
        res.send(response);
      } else {
        DB.GetOneDocument('maincategories', {_id:req.body.id}, {}, {}, function(err, result1) {
            if(err) {
                res.send(response);
            } else {
                  const data = {
                    id         : result1._id,
                    name       : result1.name,
                    image      : result1.image,
                    icon       : result1.icon,
                    status     : result1.status
                  }
                  response.status  = 1;
                  response.message = 'Main Category updated successfully';
                  response.data    = data;
                res.send(response);
            }
        });
      }
    });
  }
})

Router.post('/deleteMaincategory',function(req,res) {
  const response = {
    status  : 0,
    message : 'Something went wrong in your code!'
  }
  DB.DeleteDocument('maincategories', {_id:req.body.id}, function(err, result) {
      if(err) {
          res.send(response);
      } else {
        DB.GetDocument('maincategories', {}, {}, {}, function(err, result) {
            if(err) {
                res.send(response);
            } else {
                  response.status  = 1;
                  response.message = 'Main Category deleted successfully';
                  response.data    = result;
                  response.count   = result.length;
                  res.send(response);
            }
        });
      }
  });
})

Router.get('/listSubcategories',function(req,res) {
  const response = {
    status  : 0,
  }
  const options    = {};
  options.populate = 'parentID';
  DB.GetDocument('subcategories', {}, {}, {options}, function(err, result) {
      if(err) {
          res.send(response);
      } else {
          response.status  = 1;
          response.data    = result;
          response.count   = result.length;
          res.send(response);
      }
  });
});

Router.post('/viewSubcategory',function(req,res) {
  const response = {
    status  : 0,
  }
  DB.GetOneDocument('subcategories', {_id:req.body.id}, {}, {}, function(err, result) {
      if(err) {
          res.send(response);
      } else {
          response.status  = 1;
          response.data    = result;
          res.send(response);
      }
  });
});

var cpUpload = UPLOAD.fields([{ name: 'image', maxCount: 1 },{ name: 'icon', maxCount: 1 }])
Router.post('/addUpdateSubcategory',cpUpload,function(req,res) {
  const response = {
    status  : 0,
    message : 'Something went wrong in your code!'
  }
  req.checkBody('name', 'name is required.').notEmpty();
  var errors = req.validationErrors();
  if (errors) {
    return res.status(422).json({ errors: errors});
  }
  const parentID = req.body.parentID;
  const name     = req.body.name;
  const status   = req.body.status;
  const formData = {
    parentID    : parentID,
    name        : HELPERFUNC.Capitalize(name),
    status      : status
  }
  if(req.files['icon']){
    formData.icon  = req.files['icon'][0].filename
  } else {
    formData.icon  = req.body.icon
  }
  if(req.files['image']){
    formData.image  = req.files['image'][0].filename
  } else {
    formData.image  = req.body.image
  }
  if(req.body.id=="0"){
    DB.GetOneDocument('subcategories', {name : name}, {}, {}, function(err, result) {
      if(result){
        response.status  = 0;
        response.message = 'Data you have entered is already exist!';
        res.send(response);
      } else {
        DB.InsertDocument('subcategories', formData, function(err, result1) {
          if(err) {
            res.send(response);
          } else {
            response.status  = 1;
            response.message = 'Sub-Category added successfully';
            response.id      = result1._id;
            res.send(response);
          }
        });
      }
    });
  } else {
    DB.FindUpdateDocument('subcategories',{_id:req.body.id}, formData, function(err, result) {
      if(err) {
        res.send(response);
      } else {
        DB.GetOneDocument('subcategories', {_id:req.body.id}, {}, {}, function(err, result1) {
            if(err) {
                res.send(response);
            } else {
                  const data = {
                    id         : result1._id,
                    name       : result1.name,
                    image      : result1.image,
                    icon       : result1.icon,
                    parentID   : result1.parentID,
                    status     : result1.status
                  }
                  response.status  = 1;
                  response.message = 'Sub-Category updated successfully';
                  response.data    = data;
                res.send(response);
            }
        });
      }
    });
  }
})

Router.post('/deleteSubcategory',function(req,res) {
  const response = {
    status  : 0,
    message : 'Something went wrong in your code!'
  }
  DB.DeleteDocument('subcategories', {_id:req.body.id}, function(err, result) {
      if(err) {
          res.send(response);
      } else {
        const options    = {};
        options.populate = 'parentID';
        DB.GetDocument('subcategories', {}, {}, {options}, function(err, result) {
            if(err) {
                res.send(response);
            } else {
                  response.status  = 1;
                  response.message = 'Sub-Category deleted successfully';
                  response.data    = result;
                  response.count   = result.length;
                  res.send(response);
            }
        });
      }
  });
})

Router.get('/listCategories',function(req,res) {
  const response = {
    status  : 0,
  }
  const options    = {};
  options.populate = 'maincategory subcategory';
  DB.GetDocument('categories', {}, {}, options, function(err, result) {
      if(err) {
          res.send(response);
      } else {
          response.status  = 1;
          response.data    = result;
          response.count   = result.length;
          res.send(response);
      }
  });
});

Router.post('/viewCategory',function(req,res) {
  const response = {
    status  : 0,
  }
  DB.GetOneDocument('categories', {_id:req.body.id}, {}, {}, function(err, result) {
      if(err) {
          res.send(response);
      } else {
        DB.GetDocument('subcategories', {parentID:result.maincategory}, {}, {}, function(err, result1) {
          if(err) {
              res.send(response);
          } else {
                response.status  = 1;
                response.data    = result;
                response.subcategories = result1;
                res.send(response);
          }
        });
      }
  });
});

var cpUpload = UPLOAD.fields([{ name: 'image', maxCount: 1 },{ name: 'icon', maxCount: 1 }])
Router.post('/addUpdateCategory',cpUpload,function(req,res) {
  const response = {
    status  : 0,
    message : 'Something went wrong in your code!'
  }
  req.checkBody('name', 'name is required.').notEmpty();
  var errors = req.validationErrors();
  if (errors) {
    return res.status(422).json({ errors: errors});
  }
  const name     = req.body.name;
  const status   = req.body.status;
  const formData = {
    maincategory    :  req.body.maincategory,
    subcategory     :  req.body.subcategory,
    sort            :  req.body.sort,
    name            : HELPERFUNC.Capitalize(name),
    status          : status
  }
  if(req.files['icon']){
    formData.icon  = req.files['icon'][0].filename
  } else {
    formData.icon  = req.body.icon
  }
  if(req.files['image']){
    formData.image  = req.files['image'][0].filename
  } else {
    formData.image  = req.body.image
  }
  if(req.body.id=="0"){
    var slug = name;
    slug = slug.replace(/[^\w\-]+/g, "-");
    slug = slug.toLowerCase();
    formData.slug = slug;
    DB.GetOneDocument('categories', {name : req.body.name}, {}, {}, function(err, result) {
      if(result){
        response.status  = 0;
        response.message = 'Category name already exist!';
        res.send(response);
      } else {
        DB.GetOneDocument('categories', {sort : req.body.sort}, {}, {}, function(err, result) {
          if(result){
            response.status  = 0;
            response.message = 'Sorting order already exist!';
            res.send(response);
          } else {
            DB.InsertDocument('categories', formData, function(err, result1) {
              if(err) {
                res.send(response);
              } else {
                const options    = {};
                options.populate = 'maincategory subcategory';
                DB.GetDocument('categories', {}, {}, options, function(err, result2) {
                    if(err) {
                        res.send(response);
                    } else {
                        response.status  = 1;
                        response.message = 'Category added successfully';
                        response.data    = result2;
                        res.send(response);
                    }
                });
              }
            });
          }
        });
      }
    })
  } else {
    DB.GetOneDocument('categories', {$and:[{sort : req.body.sort},{_id:{$ne:req.body.id}}]}, {}, {}, function(err, result) {
      if(result){
        response.status  = 0;
        response.message = 'Sorting order already exist!';
        res.send(response);
      } else {
        DB.FindUpdateDocument('categories',{_id:req.body.id}, formData, function(err, result) {
          if(err) {
            res.send(response);
          } else {
            DB.GetOneDocument('categories', {_id:req.body.id}, {}, {}, function(err, result1) {
                if(err) {
                    res.send(response);
                } else {
                  DB.GetDocument('subcategories', {maincategory:result1.maincategory}, {}, {}, function(err, result2) {
                    if(err) {
                        res.send(response);
                    } else {
                          const data = {
                            id              : result1._id,
                            name            : result1.name,
                            image           : result1.image,
                            icon            : result1.icon,
                            maincategory    : result1.maincategory,
                            subcategory     : result1.subcategory,
                            sort            : result1.sort,
                            status          : result1.status
                          }
                          response.status  = 1;
                          response.message = 'Category updated successfully';
                          response.data    = data;
                          response.subcategories = result2;
                          res.send(response);
                    }
                  });
                }
            });
          }
        });
      }
    });
  }
})

Router.post('/deleteCategory',function(req,res) {
  const response = {
    status  : 0,
    message : 'Something went wrong in your code!'
  }
  DB.DeleteDocument('categories', {_id:req.body.id}, function(err, result) {
      if(err) {
          res.send(response);
      } else {
        const options    = {};
        options.populate = 'maincategory subcategory';
        DB.GetDocument('categories', {}, {}, {options}, function(err, result) {
            if(err) {
                res.send(response);
            } else {
                  response.status  = 1;
                  response.message = 'Category deleted successfully';
                  response.data    = result;
                  response.count   = result.length;
                  res.send(response);
            }
        });
      }
  });
})

module.exports = Router;
