const express       = require('express');
const Router        = express.Router();
const DB            = require('../../models/db');
const HELPERFUNC    = require('../../models/commonfunctions');
const UPLOAD        = require('../../models/fileupload');

Router.get('/listBlogCategory',function(req,res) {
  const response = {
    status  : 0,
  }
  DB.GetDocument('blogcategories', {}, {}, {}, function(err, result) {
      if(err) {
          res.send(response);
      } else {
          response.status  = 1;
          response.data    = result;
          res.send(response);
      }
  });
});

Router.post('/viewBlogcategory',function(req,res) {
  const response = {
    status  : 0,
  }
  DB.GetOneDocument('blogcategories', {_id:req.body.id}, {}, {}, function(err, result) {
      if(err) {
          res.send(response);
      } else {
          response.status  = 1;
          response.data    = result;
          res.send(response);
      }
  });
});

Router.post('/addUpdateBlogCategory',UPLOAD.single('image'),function(req,res) {
  const response = {
    status  : 0,
    message : 'Something went wrong in your code!'
  }
  req.checkBody('title', 'title is required.').notEmpty();
  var errors = req.validationErrors();
  if (errors) {
    return res.status(422).json({ errors: errors});
  }
  const title        = HELPERFUNC.Capitalize(req.body.title);
  const description = req.body.description;
  const status      = req.body.status;
  const formData = {
    title        : title,
    status      : status
  }
  if(req.file){
    formData.image  = req.file.filename
  } else if(req.body.image){
    formData.image  = req.body.image
  }
  if(req.body.id=="0"){
    DB.GetOneDocument('blogcategories', {title : title}, {}, {}, function(err, result) {
      if(result){
        response.status  = 0;
        response.message = 'Blog Category already exist!';
        res.send(response);
      } else {
        DB.InsertDocument('blogcategories', formData, function(err, result1) {
          if(err) {
            res.send(response);
          } else {
            response.status  = 1;
            response.message = 'Blog category added successfully';
            response.id      = result1._id;
            res.send(response);
          }
        });
      }
    });
  } else {
    DB.FindUpdateDocument('blogcategories',{_id:req.body.id}, formData, function(err, result) {
      if(err) {
        res.send(response);
      } else {
        DB.GetOneDocument('blogcategories', {_id:req.body.id}, {}, {}, function(err, result1) {
            if(err) {
                res.send(response);
            } else {
                  const data = {
                    id            : result1._id,
                    title          : result1.title,
                    image         : result1.image?result1.image:"",
                    status        : result1.status
                  }
                  response.status  = 1;
                  response.message = 'Blog category updated successfully';
                  response.data    = data;
                res.send(response);
            }
        });
      }
    });
  }
})

Router.post('/deleteBlogCategory',function(req,res) {
  const response = {
    status  : 0,
    message : 'Something went wrong in your code!'
  }
  DB.DeleteDocument('blogcategories', {_id:req.body.id}, function(err, result) {
      if(err) {
          res.send(response);
      } else {
        DB.GetDocument('blogcategories', {}, {}, {}, function(err, result1) {
            if(err) {
                res.send(response);
            } else {
                  response.status  = 1;
                  response.message = 'Blog category deleted successfully';
                  response.data    = result1;
                  res.send(response);
            }
        });
      }
  });
})

Router.get('/listBlogs',function(req,res) {
  const response = {
    status  : 0,
  }
  DB.GetDocument('blogs', {}, {}, {}, function(err, result) {
      if(err) {
          res.send(response);
      } else {
          response.status  = 1;
          response.data    = result;
          response.count   = result.length;
          res.send(response);
      }
  });
});

Router.post('/viewBlog',function(req,res) {
  const response = {
    status  : 0,
  }
  DB.GetOneDocument('blogs', {_id:req.body.id}, {}, {}, function(err, result) {
      if(err) {
          res.send(response);
      } else {
          response.status  = 1;
          response.data    = result;
          res.send(response);
      }
  });
});

Router.post('/addUpdateBlog',UPLOAD.single('image'),function(req,res) {
  const response = {
    status  : 0,
    message : 'Something went wrong in your code!'
  }
  req.checkBody('title', 'title is required.').notEmpty();
  var errors = req.validationErrors();
  if (errors) {
    return res.status(422).json({ errors: errors});
  }
  const title           = HELPERFUNC.Capitalize(req.body.title);
  const metadescription = req.body.metadescription;
  const blogcategory    = req.body.blogcategory;
  const metakeyword     = req.body.metakeyword;
  const content         = req.body.content;
  const status          = req.body.status;
  const formData = {
    title           : title,
    metadescription : metadescription,
    blogcategory    : blogcategory,
    metakeyword     : metakeyword,
    content         : content,
    status          : status,
    feature         : req.body.feature,
  }
  if(req.file){
    formData.image  = req.file.filename
  } else {
    formData.image  = req.body.image
  }
  if(req.body.id=="0"){
    var slug = title;
    slug = slug.replace(/[^\w\-]+/g, "-");
    slug = slug.toLowerCase();
    formData.slug = slug;
    DB.GetOneDocument('blogs', {title : title}, {}, {}, function(err, result) {
      if(result){
        response.status  = 0;
        response.message = 'Data you have entered is already exist!';
        res.send(response);
      } else {
        DB.InsertDocument('blogs', formData, function(err, result1) {
          if(err) {
            res.send(response);
          } else {
            response.status  = 1;
            response.message = 'Blog added successfully';
            response.id      = result1._id;
            res.send(response);
          }
        });
      }
    });
  } else {
    DB.FindUpdateDocument('blogs',{_id:req.body.id}, formData, function(err, result) {
      if(err) {
        res.send(response);
      } else {
        DB.GetOneDocument('blogs', {_id:req.body.id}, {}, {}, function(err, result1) {
            if(err) {
                res.send(response);
            } else {
                  // const data = {
                  //   id            : result1._id,
                  //   title          : result1.title,
                  //   image         : result1.image,
                  //   metadescription   : result1.metadescription,
                  //   metakeyword   : result1.metakeyword,
                  //   content   : result1.content,
                  //   blogcategory   : result1.blogcategory,
                  //   status        : result1.status
                  // }
                  response.status  = 1;
                  response.message = 'Blog updated successfully';
                  response.data    = result1;
                res.send(response);
            }
        });
      }
    });
  }
})

Router.post('/deleteBlog',function(req,res) {
  const response = {
    status  : 0,
    message : 'Something went wrong in your code!'
  }
  DB.DeleteDocument('blogs', {_id:req.body.id}, function(err, result) {
      if(err) {
          res.send(response);
      } else {
        DB.GetDocument('blogs', {}, {}, {}, function(err, result1) {
            if(err) {
                res.send(response);
            } else {
                  response.status  = 1;
                  response.message = 'Blog deleted successfully';
                  response.data    = result1;
                  response.count   = result1.length;
                  res.send(response);
            }
        });
      }
  });
})
module.exports = Router;
