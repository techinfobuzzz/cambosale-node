const express       = require('express');
const Router        = express.Router();
const DB            = require('../../models/db');
const UPLOAD        = require('../../models/fileupload');
const HELPERFUNC    = require('../../models/commonfunctions');

var cpUpload = UPLOAD.fields([{ name: 'image', maxCount: 1 },{ name: 'coverpicture', maxCount: 1 }])
Router.post('/addUpdateShop',cpUpload,function(req,res) {
  const response = {
    status  : 0,
    message : 'Something went wrong in your code!'
  }
  req.checkBody('shopname', 'shopname is required.').notEmpty();
  var errors = req.validationErrors();
  if (errors) {
    return res.status(422).json({ errors: errors});
  }
  const shopname = req.body.shopname;
  const formData = {
    shopname                : HELPERFUNC.Capitalize(shopname),
    slug                    : req.body.slug,
    rangelocation           : req.body.rangelocation,
    distance                : req.body.distance,
    category                : req.body.category,
    accountdetails          : req.body.accountdetails,
    delivery                : req.body.delivery,
    contactnumber           : req.body.contactnumber,
    email                   : req.body.email,
    locationaddress         : req.body.locationaddress,
    description             : req.body.description,
    location                : {
      lat : req.body.locationlat,
      lon : req.body.locationlon,
    },
  }
  if(req.files['coverpicture']){
    formData.coverpicture  = req.files['coverpicture'][0].filename
  } else {
    formData.coverpicture  = req.body.coverpicture
  }
  if(req.files['image']){
    formData.image  = req.files['image'][0].filename
  } else {
    formData.image  = req.body.image
  }
  if(req.body.id=="0"){
    DB.GetOneDocument('shops', {slug : req.body.slug}, {}, {}, function(err, result) {
      if(result){
        response.status  = 0;
        response.message = 'Data you have entered is already exist!';
        res.send(response);
      } else {
        DB.InsertDocument('shops', formData, function(err, result1) {
          if(err) {
            res.send(response);
          } else {
            response.status  = 1;
            response.message = 'Shop added successfully';
            response.id      = result1._id;
            res.send(response);
          }
        });
      }
    });
  } else {
    DB.FindUpdateDocument('shops',{_id:req.body.id}, formData, function(err, result) {
      if(err) {
        res.send(response);
      } else {
        DB.GetOneDocument('shops', {_id:req.body.id}, {}, {}, function(err, result1) {
            if(err) {
                res.send(response);
            } else {
                  const data = {
                    id                      : result1._id,
                    shopname                : result1.shopname,
                    slug                    : result1.slug,
                    rangelocation           : result1.rangelocation,
                    distance                : result1.distance,
                    category                : result1.category,
                    accountdetails          : JSON.parse(result1.accountdetails),
                    delivery                : result1.delivery,
                    contactnumber           : result1.contactnumber,
                    email                   : result1.email,
                    locationaddress         : result1.locationaddress,
                    description             : result1.description,
                    locationlat             : result1.location.lat,
                    locationlon             : result1.location.lon,
                    image                   : result1.image,
                    coverpicture            : result1.coverpicture,
                  }
                  response.status  = 1;
                  response.message = 'Shop updated successfully';
                  response.data    = data;
                res.send(response);
            }
        });
      }
    });
  }
})

Router.get('/listShops',function(req,res) {
  const response = {
    status  : 0,
  }
  DB.GetDocument('shops', {}, {}, {}, function(err, result) {
      if(err) {
          res.send(response);
      } else {
          response.status  = 1;
          response.data    = result;
          response.count   = result.length;
          res.send(response);
      }
  });
});

Router.post('/viewShop',function(req,res) {
  const response = {
    status  : 0,
  }
  DB.GetOneDocument('shops', {_id:req.body.id}, {}, {}, function(err, result1) {
      if(err) {
          res.send(response);
      } else {
          const data = {
            id                      : result1._id,
            shopname                : result1.shopname,
            slug                    : result1.slug,
            rangelocation           : result1.rangelocation,
            distance                : result1.distance,
            category                : result1.category,
            accountdetails          : JSON.parse(result1.accountdetails),
            delivery                : result1.delivery,
            contactnumber           : result1.contactnumber,
            email                   : result1.email,
            locationaddress         : result1.locationaddress,
            description             : result1.description,
            locationlat             : result1.location.lat,
            locationlon             : result1.location.lon,
            image                   : result1.image,
            coverpicture            : result1.coverpicture,
          }
          response.status  = 1;
          response.data    = data;
          res.send(response);
      }
  });
});

Router.post('/deleteShop',function(req,res) {
  const response = {
    status  : 0,
    message : 'Something went wrong in your code!'
  }
  DB.DeleteDocument('shops', {_id:req.body.id}, function(err, result) {
      if(err) {
          res.send(response);
      } else {
        DB.GetDocument('shops', {}, {}, {}, function(err, result1) {
            if(err) {
                res.send(response);
            } else {
                  response.status  = 1;
                  response.message = 'Shop deleted successfully';
                  response.data    = result1;
                  response.count   = result1.length;
                  res.send(response);
            }
        });
      }
  });
})

module.exports = Router;
