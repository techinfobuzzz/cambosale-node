const express       = require('express');
const Router        = express.Router();
const DB            = require('../../models/db');
const HELPERFUNC    = require('../../models/commonfunctions');
var mongoose        = require('mongoose');
var path            = require('path');
var fs              = require('fs');

Router.get('/listLanguages',function(req,res) {
  const response = {
    status  : 0,
  }
  DB.GetDocument('languages', {}, {}, {}, function(err, result) {
      if(err) {
          res.send(response);
      } else {
          response.status  = 1;
          response.data    = result;
          response.count   = result.length;
          res.send(response);
      }
  });
});

Router.post('/viewLanguage',function(req,res) {
  const response = {
    status  : 0,
  }
  DB.GetOneDocument('languages', {_id:req.body.id}, {}, {}, function(err, result) {
      if(err) {
          res.send(response);
      } else {
          response.status  = 1;
          res.send(response);
      }
  });

  DB.GetOneDocument('languages', {_id:new mongoose.Types.ObjectId(req.body.id)}, {}, {}, function(err, languageDetails) {
		if(err){
			console.log("DB Error", err);
			kickback.ack = "Something went wrong !!. Try again later";
			res.send(kickback)
		}else{
			var fileName = languageDetails.code+'_lang.json';
			var filepath = path.join(__dirname,'../..','lang/');
			fs.readFile(filepath+fileName,'utf8',function(err,data){
				if(err){
					console.log("FILE Error", err);
					kickback.ack = "Something went wrong !!. Try again later";
					res.send(kickback)
				}else{
					var languageData = JSON.parse(data);
					var defaultFilePath = path.join(__dirname,'../..','lang/default/default.json');
					fs.readFile(defaultFilePath, 'utf8', function(err,data){
						if(err){
							kickback.ack = "Something went wrong !!. Try again later";
							res.send(kickback)
						}else{
							var DefaultLanguageData = JSON.parse(data);
							var newLangData = Object.assign(DefaultLanguageData,languageData);
							kickback.status  = 1;
							kickback.message = 'success';
              kickback.data    = languageDetails;
							kickback.languageData = newLangData;
							res.send(kickback);
						}
					})
				}
			})
		}
	})
});

Router.post('/addUpdateLanguage',function(req,res) {
  const response = {
    status  : 0,
    message : 'Something went wrong in your code!'
  }
  req.checkBody('name', 'name is required.').notEmpty();
  req.checkBody('code', 'code is required.').notEmpty();
  var errors = req.validationErrors();
  if (errors) {
    return res.status(422).json({ errors: errors});
  }
  const name     = req.body.name;
  const code     = req.body.code;
  const status   = req.body.status;
  const formData = {
    name      : HELPERFUNC.Capitalize(name),
    code      : HELPERFUNC.UpperCase(code),
    status    : status
  }
  if(req.body.id=="0"){
    DB.GetOneDocument('languages', {name : name}, {}, {}, function(err, result) {
      if(result){
        response.status  = 0;
        response.message = 'Data you have entered is already exist!';
        res.send(response);
      } else {
        DB.InsertDocument('languages', formData, function(err, result1) {
          if(err) {
            res.send(response);
          } else {
            response.status  = 1;
            response.message = 'Language added successfully';
            response.id      = result1._id;
            res.send(response);
          }
        });
      }
    });
  } else {
    DB.FindUpdateDocument('languages',{_id:req.body.id}, formData, function(err, result) {
      if(err) {
        res.send(response);
      } else {
        DB.GetOneDocument('languages', {_id:req.body.id}, {}, {}, function(err, result1) {
            if(err) {
                res.send(response);
            } else {
                  const data = {
                    id         : result1._id,
                    name       : result1.name,
                    code       : result1.code,
                    status     : result1.status
                  }
                  response.status  = 1;
                  response.message = 'Language updated successfully';
                  response.data    = data;
                res.send(response);
            }
        });
      }
    });
  }
})

Router.post('/deleteLanguage',function(req,res) {
  const response = {
    status  : 0,
    message : 'Something went wrong in your code!'
  }
  DB.DeleteDocument('languages', {_id:req.body.id}, function(err, result) {
      if(err) {
          res.send(response);
      } else {
        DB.GetDocument('languages', {}, {}, {}, function(err, result) {
            if(err) {
                res.send(response);
            } else {
                  response.status  = 1;
                  response.message = 'Language deleted successfully';
                  response.data    = result;
                  response.count   = result.length;
                  res.send(response);
            }
        });
      }
  });
})

Router.post('/view',function(req,res) {
  const kickback = {
    status  : 0,
    message : 'Something went wrong in your code!'
  }
    DB.GetOneDocument('languages', {_id:new mongoose.Types.ObjectId(req.body.id)}, {}, {}, function(err, languageDetails) {
		if(err){
			console.log("DB Error", err);
			kickback.ack = "Something went wrong !!. Try again later";
			res.send(kickback)
		}else{
			var fileName = languageDetails.code+'_lang.json';
			var filepath = path.join(__dirname,'../..','lang/');
			fs.readFile(filepath+fileName,'utf8',function(err,data){
				if(err){
					console.log("FILE Error", err);
					kickback.ack = "Something went wrong !!. Try again later";
					res.send(kickback)
				}else{
					var languageData = JSON.parse(data);
					var defaultFilePath = path.join(__dirname,'../..','lang/default/default.json');
					fs.readFile(defaultFilePath, 'utf8', function(err,data){
						if(err){
							kickback.ack = "Something went wrong !!. Try again later";
							res.send(kickback)
						}else{
							var DefaultLanguageData = JSON.parse(data);
							var newLangData = Object.assign(DefaultLanguageData,languageData);
							kickback.status  = 1;
							kickback.message = 'success';
							kickback.languageData = newLangData;
							res.send(kickback);
						}
					})
				}
			})
		}
	})
})

module.exports = Router;
