const express       = require('express');
const Router        = express.Router();
const DB            = require('../../models/db');

Router.get('/listMaincategories',function(req,res) {
    const response = {
      status  : 0,
    }
    DB.GetDocument('maincategories', {}, {}, {}, function(err, result) {
        if(err) {
            res.send(response);
        } else {
            response.status  = 1;
            response.data    = result;
            response.count   = result.length;
            res.send(response);
        }
    });
});

Router.get('/listSubcategories',function(req,res) {
    const response = {
      status  : 0,
    }
    const options    = {};
    options.populate = 'parentID';
    DB.GetDocument('subcategories', {}, {}, {options}, function(err, result) {
        if(err) {
            res.send(response);
        } else {
            response.status  = 1;
            response.data    = result;
            response.count   = result.length;
            res.send(response);
        }
    });
});

Router.get('/listCategories',function(req,res) {
    const response = {
      status  : 0,
    }
    const options    = {};
    options.populate = 'maincategory subcategory';
    DB.GetDocument('categories', {status:1}, {}, {options}, function(err, result) {
        if(err) {
            res.send(response);
        } else {
            response.status  = 1;
            response.data    = result;
            response.count   = result.length;
            res.send(response);
        }
    });
});

module.exports = Router;
