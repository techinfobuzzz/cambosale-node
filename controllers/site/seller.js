const express       = require('express');
const Router        = express.Router();
var jwt             = require('jsonwebtoken');
const async         = require("async");
const DB            = require('../../models/db');
const UPLOAD        = require('../../models/fileupload');

Router.post('/viewShopSeller',function(req,res) {
  DB.GetOneDocument('shops', {slug:req.body.shopslug}, {}, {}, function(err, result) {
    if(result){
      async.parallel([
        function(callback) {
          const options    = {};
          options.populate = 'maincategory subcategory category shop brand city';
          DB.GetDocument('products', {shop:result._id}, {}, options, function(err, productsresult) {
                  callback(null,productsresult);
          });
        },
        function(callback) {
          const options = { populate : "shopfollower userfollower" }
          DB.GetDocument('followers', { shopfollowing : result._id }, {}, options , function(err, followers) {
            callback(null, followers);
          });
        },
        function(callback) {
          const options = { populate : "shopfollowing userfollowing" }
          DB.GetDocument('followers', { shopfollower : result._id }, {}, options , function(err, following) {
            callback(null, following);
          });
        },
        function(callback) {
          const Query = [ {
                            $match: { shop:result._id} ,
                          },
                          {
                            $project:{category:1}
                          },
                          { 
                            $group:  { _id: { category: "$category"},"doc":{"$first":"$$ROOT"}}
                          },
                            {"$replaceRoot":{"newRoot":"$doc"}}
                        ];
           const options = { populate : "category" }
           DB.GetAggregation('products', Query, options, function(err, categories) {
            callback(null, categories);
          })
        },
      ],
      function(err, results) {
        const response = {
          products     : results[0],
          followers    : results[1],
          following    : results[2],
          categories   : results[3],
          shop         : result,
        }
        res.send({status:1,data:response})
      });
    }
  });
})

Router.post('/addFollower',function(req,res) {
  const response = {
    status  : 0,
    message : 'Something went wrong in your code!'
  }
  var formData = {shopfollowing:req.body.shopfollowing}
  if(req.body.userfollowing){
    formData = {userfollowing:req.body.userfollowing}
  }
  if(req.body.userfollower){
    formData.userfollower = req.body.userfollower
  }
  if(req.body.shopfollower){
    formData.shopfollower = req.body.shopfollower
  }
  DB.InsertDocument('followers', formData, function(err, result1) {
    if(err) {
      res.send(response);
    } else {
      async.parallel([
        function(callback) {
          if(req.body.userfollower ){
            const options = { populate : "shopfollowing userfollowing shopfollower userfollower" }
            DB.GetDocument('followers', { userfollower : req.body.userfollower }, {}, options , function(err, userfollowing) {
              callback(null, userfollowing);
            });
          } else if(req.body.userid){
            const options = { populate : "shopfollowing userfollowing shopfollower userfollower" }
            DB.GetDocument('followers', { userfollower : req.body.userid }, {}, options , function(err, userfollowing) {
              callback(null, userfollowing);
            });
          }
        },
        function(callback) {
          if(req.body.shopfollower){
            const options = { populate : "shopfollower userfollower shopfollowing userfollowing" }
            DB.GetDocument('followers', { shopfollower : req.body.shopfollower }, {}, options , function(err, shopfollowing) {
              callback(null, shopfollowing);
            });
          } else if(req.body.shopid){
            const options = { populate : "shopfollower userfollower shopfollowing userfollowing" }
            DB.GetDocument('followers', { shopfollower : req.body.shopid }, {}, options , function(err, shopfollowing) {
              callback(null, shopfollowing);
            });
          }  else {
            callback(null, []);
          }
        },
      ],
      function(err, results) {
        const response = {
          userfollowing    : results[0],
          shopfollowing    : results[1],
        }
        res.send({status:1,data:response})
      });
    }
  });
})

Router.post('/removeFollower',function(req,res) {
  const response = {
    status  : 0,
    message : 'Something went wrong in your code!'
  }
  DB.DeleteDocument('followers', {_id:req.body.followId}, function(err, result) {
      if(err) {
          res.send(response);
      } else {
        async.parallel([
          function(callback) {
            const options = { populate : "shopfollowing userfollowing" }
            DB.GetDocument('followers', { userfollower : req.body.user }, {}, options , function(err, userfollowing) {
              callback(null, userfollowing);
            });
          },
          function(callback) {
            if(req.body.shop){
              const options = { populate : "shopfollowing userfollowing" }
              DB.GetDocument('followers', { shopfollower : req.body.shop }, {}, options , function(err, shopfollowing) {
                callback(null, shopfollowing);
              });
            } else {
              callback(null, []);
            }
          },
        ],
        function(err, results) {
          const response = {
            userfollowing    : results[0],
            shopfollowing    : results[1],
          }
          res.send({status:1,data:response})
        });
      }
  });
})

Router.post('/viewUserSeller',function(req,res) {
      async.parallel([
        function(callback) {
          const options    = {};
          options.populate = 'maincategory subcategory category shop brand city';
          DB.GetDocument('products', {user:req.body.user}, {}, options, function(err, productsresult) {
                  callback(null,productsresult);
          });
        },
        function(callback) {
          const options = { populate : "shopfollower userfollower" }
          DB.GetDocument('followers', { userfollowing : req.body.user }, {}, options , function(err, followers) {
            callback(null, followers);
          });
        },
        function(callback) {
          const options = { populate : "shopfollowing userfollowing" }
          DB.GetDocument('followers', { userfollower : req.body.user }, {}, options , function(err, following) {
            callback(null, following);
          });
        },
        function(callback) {
          const options = { populate : "productId productUser productShop" }
          DB.GetDocument('likedproducts', { userId : req.body.user }, {}, options , function(err, liked) {
            callback(null, liked);
          });
        },
        function(callback) {
          DB.GetOneDocument('users', {_id:req.body.user}, {}, {}, function(err, user) {
                  callback(null,user);
          });
        },
        function(callback) {
          DB.GetOneDocument('users', {_id:req.body.user}, {}, {}, function(err, userresult) {
              if(userresult){
                DB.GetOneDocument('shops', {contactnumber:userresult.phonenumber}, {}, {}, function(err, shops) {
                  callback(null,shops);
                  });
              } else {
                callback(null,{});
              }
          });
        },
      ],
      function(err, results) {
        if(results){
          const response = {
            products     : results[0],
            followers    : results[1],
            following    : results[2],
            liked        : results[3],
            user         : results[4],
            shop         : results[5],
          }
          res.send({status:1,data:response})
        } else {
          res.send({status:0})
        }
      });
})

module.exports = Router;
