var mongoose = require('mongoose');
var Schema   = mongoose.Schema;

var CATEGORYSCHEMA = {
    blogcategory  : { type: Schema.Types.ObjectId, ref: 'blogcategories' },
    title         : String,
    image         : String,
    metakeyword   : String,
    metadescription   : String,
    content       : String,
    slug          : String,
    feature       : String,
    status        : Boolean,
};

module.exports = CATEGORYSCHEMA;
