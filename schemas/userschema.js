var USERSCHEMA = {
    fullname                : String,
    email                   : String,
    location                : String,
    phonenumber             : String,
    password                : String,
    description             : String,
    address                 : String,
    location                : {
                        lat : Number,
                        lon : Number
                              },
    verification            : {
    emailVerification       : Boolean,
    facebookVerification    : Boolean,
    phonenumberVerification : Boolean,
                              },
    profileimage            : String,
    coverpicture            : String
                          
};

module.exports = USERSCHEMA;
