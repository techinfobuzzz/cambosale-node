var mongoose = require('mongoose');
var Schema   = mongoose.Schema;

var CATEGORYSCHEMA = {
    maincategory  : { type: Schema.Types.ObjectId, ref: 'maincategories' },
    subcategory   : { type: Schema.Types.ObjectId, ref: 'subcategories' },
    name          : String,
    slug          : String,
    icon          : String,
    image         : String,
    sort          : Number,
    status        : Boolean,
};

module.exports = CATEGORYSCHEMA;
