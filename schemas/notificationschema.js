var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var NOTIFICATIONSCHEMA = {
    productid: { type: Schema.Types.ObjectId, ref: 'products' },
    userid: { type: Schema.Types.ObjectId, ref: 'users' },
    ownerid: { type: Schema.Types.ObjectId, ref: 'shops' },
    seen: Boolean
};

module.exports = NOTIFICATIONSCHEMA;
