var mongoose = require('mongoose');
var Schema   = mongoose.Schema;
var FAQTRANSLATIONCHEMA = {
    faqid      : { type: Schema.Types.ObjectId, ref: 'faqs' },
    locale     : String,
    question   : String,
    answer     : String,
    status     : Boolean,
};

module.exports = FAQTRANSLATIONCHEMA;
