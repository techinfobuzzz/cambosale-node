var CONTACTSCHEMA = {
    name: String,
    email: String,
    mobilenumber: String,
    subject: String,
    message: String,
};

module.exports = CONTACTSCHEMA;
