var NEWSLETTERSCHEMA = {
    templatename   : String,
    emailsubject   : String,
    emailcontent   : String,
    status         : Boolean,
};

module.exports = NEWSLETTERSCHEMA;
