var CURRENCYSCHEMA = {
    name        : String,
    symbol      : String,
    status      : Boolean,
};

module.exports = CURRENCYSCHEMA;
