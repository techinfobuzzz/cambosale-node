var SHOPSCHEMA = {
    shopname           : String,
    slug               : String,
    rangelocation      : String,
    distance           : String,
    category           : String,
    accountdetails     : [],
    delivery           : String,
    contactnumber      : String,
    email              : String,
    locationaddress    : String,
    image              : String,
    coverpicture       : String,
    description        : String,
    bankaccount        : String,
    location     : {
        lat: Number,
        lon: Number
    },
};

module.exports = SHOPSCHEMA;
