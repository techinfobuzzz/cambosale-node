var SELLERSCHEMA = {
    name           : String,
    logo           : String,
    contactnumber  : String,
    email          : String,
    location       : String,
};

module.exports = SELLERSCHEMA;
