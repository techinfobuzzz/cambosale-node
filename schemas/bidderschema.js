var mongoose = require('mongoose');
var Schema   = mongoose.Schema;

var BIDDERSCHEMA = {
    user         : { type: Schema.Types.ObjectId, ref: 'users' },
    product      : { type: Schema.Types.ObjectId, ref: 'products' },
    amount       : String,
    biddingtime  : String
};

module.exports = BIDDERSCHEMA;
