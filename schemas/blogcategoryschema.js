var mongoose = require('mongoose');
var Schema   = mongoose.Schema;

var BLOGCATEGORYSCHEMA = {
    title         : String,
    image         : String,
    status        : Boolean,
};

module.exports = BLOGCATEGORYSCHEMA;
