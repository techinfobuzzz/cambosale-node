const multer = require('multer');

const DIR = './uploads/';

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, DIR);
    },
    filename: (req, file, cb) => {
        var d          = new Date();
        var randomName = d.getTime();
        const fileName = file.originalname.toLowerCase().split(' ').join('-');
        cb(null, randomName + fileName)
    }
});

  var upload = multer({
      storage: storage,
    fileFilter: (req, file, cb) => {
            cb(null, true);
        }
});

module.exports = upload;
